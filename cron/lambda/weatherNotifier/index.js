const AWS = require ("aws-sdk");
const moment = require("moment");
const s3 = new AWS.S3();
const ddb = new AWS.DynamoDB.DocumentClient();
const ses = new AWS.SES();
module.exports.getObject = async (event) =>{
    console.log(JSON.stringify(event));
    const {Records: SNSRecords} = event;
    const {Sns: {Message: snsMesssage}} = SNSRecords[0];

    const s3Event = JSON.parse(snsMesssage);
    const { Records } = s3Event;
    const {s3: { bucket: {name: Bucket}, object: {key: Key}}} = Records[0];
   const data = await s3.getObject({
       Bucket,
       Key
    }).promise();
   console.log(JSON.parse(data.Body.toString()));
   const weatherForecast = (JSON.parse(data.Body.toString()));
    const {city:{name: CityName, country: CountryCode}} = weatherForecast;
   const emails = await ddb.query( {
       TableName: 'Subscription',
       IndexName: 'cityName-countryCode-Index',
       ProjectionExpression: "email",
       KeyConditionExpression: 'cityName = :CN and countryCode = :CC',
       ExpressionAttributeValues: {
           ':CN': CityName,
           ':CC': CountryCode
       }
   }).promise();
   console.log(emails);
   console.log(emails.Items[0].email);
   const wl = weatherForecast.list[0];
   const {main:{temp: actual_temperature,temp_min: minimal_temperature, temp_max: maximal_temperature,humidity:humidity},wind:{speed:wind_speed},dt_txt:dt} = wl;
    const date = moment(dt).format("MMM Do YY");
      const result = await Promise.all(emails.Items.map(object => {

       return ses.sendEmail({
           Destination: {
               ToAddresses:[  object.email ] },
           Message: {
               Body: {
                   Html: {
                       Charset: "UTF-8",
                       Data:  `<html>
 <head>
 <title>Weather forcast</title><style>h1{color:#f00;}</style>
 </head>
 <body>
 <ul>
    <li>City Name: ${CityName}, ${CountryCode}</li>
     <li>Actual Temperature : ${actual_temperature}</li>
      <li>Minimal Temperature : ${minimal_temperature}</li>
       <li>Maximal Temperature : ${maximal_temperature}</li>
        <li>Humidity : ${humidity}</li>
         <li>Wind speed : ${wind_speed}</li>
         <li>Date : ${date}</li>
 </ul>
 </body>
 </html>`
                   }
               },
               Subject: {
                   Charset: 'UTF-8',
                   Data: `${CityName},${CountryCode} daily weather`
               }
           },
           Source: 'do-not-reply@phileas.tools'}).promise()
   }));

   console.log(JSON.stringify(result));
    return result;


};