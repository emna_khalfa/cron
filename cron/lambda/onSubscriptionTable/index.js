const AWS = require("aws-sdk");
const lambda = new AWS.Lambda();
const {LAMBDA_ARN_PUT_WEATHER_TARGET} = process.env;
 const filterFunction = (element) => element.eventName !== "REMOVE" ;
 const mappingFunction = (record) => {
     const {cityName,countryCode} = AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage);
     return {cityName,countryCode};
 };

module.exports.onSubscriptionTable = async (event) =>{
    console.log(JSON.stringify(event));
    const {Records} = event;
    const payload = Records
        .filter(filterFunction)
        .map(mappingFunction);
    return lambda.invoke({
        FunctionName: LAMBDA_ARN_PUT_WEATHER_TARGET,
        InvocationType: "Event",
        Payload: JSON.stringify(payload)
    }).promise();

};