const AWS = require ("aws-sdk");
const CloudWatchEvents = new AWS.CloudWatchEvents();
const {ARN_LAMBDA_GET_WEATHER} = process.env;


const transform = (object, index)=> {
    const {cityName, countryCode} = object;
    if(!cityName || !countryCode){
        console.warn(`Object ${index}: ${JSON.stringify(object)} has incorrect format`);
        return null;
    }
    return {
        Arn: ARN_LAMBDA_GET_WEATHER,
        Id: `GetWeatherLambdaFunction_${cityName}_${countryCode}`,
        Input: JSON.stringify({cityName,countryCode})
    }
};
module.exports.putWeatherTargets = async (event) => {

    if(!Array.isArray(event)){
        throw new Error('Event must be an array of objects');
    }
    return await CloudWatchEvents.putTargets({
        Rule: "dailyRate",
        Targets: event.map(transform).filter(obj => obj !== null)
    }).promise();
};