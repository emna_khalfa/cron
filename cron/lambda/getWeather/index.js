const axios = require('axios');
const AWS = require ("aws-sdk");
const s3 = new AWS.S3();
module.exports.getWeather = async ({cityName,countryCode}) => {

    const {secretKey} = process.env;
    const url = `http://api.openweathermap.org/data/2.5/forecast?q=${cityName},${countryCode}&units=metric&APPID=${secretKey}`;

        try {
            const response = await axios.get(url);
            console.log(JSON.stringify(response.data));
            return await s3.putObject({
                Bucket: 'phileas-weather-bucket',
                Key: `${cityName}_${countryCode}_weather.json`,
                Body: JSON.stringify(response.data),
                ContentType: "application/json"
            }).promise();

        } catch (error) {
            console.error(error);
            throw error;
        }

    };

